WIP: Simple orderbook for betting

TODO:

* Using smart ptrs
* tests (not just vanilla asserts)
* Use stake instead of qty as input?
* Execution storage and settling
* Use protobuf for order serialization over tcp.
* Basic heartbeat protocol over tcp.
* Valgrind
* Money handling vs float