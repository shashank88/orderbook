
#include "includes/Account.h"
#include "includes/Market.h"
#include "includes/order_book.h"
#include "includes/server.h"

#include <vector>

constexpr int TOTAL_ACCOUNTS = 3;

std::vector<Account> create_accounts(int count) {
  std::vector<Account> accounts;
  for (auto idx = 0; idx < count; idx++) {
    accounts.push_back(Account{idx, 1000.0, "Potato_" + std::to_string(idx)});
  }
  return accounts;
}

int main() {
  auto accounts =
      create_accounts(TOTAL_ACCOUNTS); // TODO: move instead of copy?
  auto market_a = Market(1, accounts);
  for (auto cid : {101, 102}) {
    market_a.add_contract(cid);
  }

  market_a.add_order(1, 101, 1, 100 * QTY_MULTIPLIER, 5043, BUY);
  market_a.add_order(1, 101, 2, 50 * QTY_MULTIPLIER, 5043, SELL);
  market_a.add_order(1, 101, 1, 50 * QTY_MULTIPLIER, 103, BUY);
  market_a.pretty_print_contract_book(101);

  market_a.add_order(1, 101, 2, 20 * QTY_MULTIPLIER, 5043, SELL);
  market_a.pretty_print_contract_book(101);

  market_a.add_order(1, 101, 1, 100 * QTY_MULTIPLIER, 4343, BUY);
  market_a.add_order(1, 101, 1, 120 * QTY_MULTIPLIER, 4343, SELL);
  market_a.add_order(1, 101, 2, 150 * QTY_MULTIPLIER, 9999, SELL);

  market_a.pretty_print_contract_book(101);

  std::cout << "\nAccount details:";
  for (const auto &acc : accounts) {
    std::cout << acc;
  }
  //  try {
  //    boost::asio::io_context io_context;
  //    server s(io_context, 8803);
  //    io_context.run();
  //  } catch (std::exception &e) {
  //    std::cerr << "Exception: " << e.what() << "\n";
  //  }

  return 0;
}