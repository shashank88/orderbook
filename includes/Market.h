//
// Created by shashank khare on 17/07/2018.
//

#ifndef ORDERBOOK_MARKET_H
#define ORDERBOOK_MARKET_H

#include "Account.h"
#include "order_book.h"
#include <string>
#include <unordered_map>

enum MarketState {
  MARKET_NEW,
  MARKET_OPEN,
  MARKET_LIVE,
  MARKET_SETTLED,
};

class Market {
private:
  int id;
  MarketState state;
  std::unordered_map<int, std::shared_ptr<OrderBook>> contract_books;
  std::vector<Account> &accounts;

public:
  Market(int id, std::vector<Account> &_account)
      : id(id), state(MARKET_NEW), accounts(_account) {}

  void add_contract(int contract_id) {
    contract_books[contract_id] = std::make_shared<OrderBook>(accounts);
  }

  bool add_order(int market_id, int contract_id, int account_id, int qty,
                 int price, order_side side) {
    return contract_books[contract_id]->add_order_qty(account_id, qty, price,
                                                      side);
  }

  void pretty_print_contract_book(int contract_id) {
    contract_books[contract_id]->pretty_print_book();
  }
};

#endif // ORDERBOOK_MARKET_H
