//
// Created by shashank khare on 17/07/2018.
//

#ifndef ORDERBOOK_ORDER_BOOK_H
#define ORDERBOOK_ORDER_BOOK_H

#include "Account.h"
#include <iostream>
#include <map>
#include <unordered_map>
#include <vector>

enum order_side { BUY, SELL };

class Order {
public:
  Order(int _account, int _qty, int _price, order_side _side, int _order_id)
      : account_id(_account), quantity(_qty), price(_price), side(_side),
        order_id(_order_id) {}
  int account_id;
  int quantity;
  int price;
  order_side side;
  int order_id;

  friend std::ostream &operator<<(std::ostream &os, const Order &ord) {
    os << "order_id: " << ord.order_id << ", Account: " << ord.account_id
       << ", price: " << ord.price << ", qty:" << ord.quantity
       << ", side: " << ord.side;
    return os;
  }

  bool is_empty() { return quantity == 0; }
};

constexpr int QTY_MULTIPLIER = 10000;
constexpr int PRICE_MULTIPLIER = 100;
using price_orders_map = std::map<int, std::vector<std::shared_ptr<Order>>>;

class OrderBook {
  price_orders_map buy_map;
  price_orders_map sell_map;
  std::unordered_map<int, std::shared_ptr<Order>> order_record;
  std::vector<Account> &accounts;
  int order_seq = 0;

  void add_execution(std::shared_ptr<Order> order, int qty, int price,
                     int other_account_id) {
    std::cout << "\n * EXECUTION => Price: " << price << ", Qty: " << qty;
    auto &order_account = accounts[order->account_id];
    auto &book_account = accounts[other_account_id];

    if (order->side == BUY) { // order_account is buy
      // use other side for potential
      order_account.potential += calc_liability(qty, price, SELL);
      book_account.potential += calc_liability(qty, price, BUY);
    } else {
      order_account.potential += calc_liability(qty, price, BUY);
      book_account.potential += calc_liability(qty, price, SELL);
    }
  }

  void clean_order_book(std::vector<std::shared_ptr<Order>> &orders) {
    orders.erase(std::remove_if(orders.begin(), orders.end(),
                                [&](std::shared_ptr<Order> ord) {
                                  return ord->is_empty();
                                }),
                 orders.end());
  }

  int cross_orders_at_price(std::shared_ptr<Order> order, int qty_rem,
                            std::vector<std::shared_ptr<Order>> &orders,
                            int price) {
    for (auto &ord : orders) {
      if (ord->quantity &&
          ord->quantity <= qty_rem) { // Full match. Mark 0 qty.
        add_execution(order, ord->quantity, price, ord->account_id);
        qty_rem -= ord->quantity;
        ord->quantity = 0; // remove erase later
      } else {
        add_execution(order, qty_rem, price, ord->account_id);
        ord->quantity = ord->quantity - qty_rem;
        qty_rem = 0;
      }
    }
    clean_order_book(orders); // TODO: Don't clean every iteration.
    return qty_rem;
  }

  void cross_orders(std::shared_ptr<Order> order) {
    int qty_rem = order->quantity;
    if (order->side == BUY) {
      for (auto &element :
           sell_map) { // Find lowest price available for buying.
        if (element.first <= order->price) {
          qty_rem = cross_orders_at_price(order, qty_rem, element.second,
                                          element.first);
        }
      }
      order->quantity = qty_rem;
      if (order->quantity) {
        buy_map[order->price].push_back(order);
      }
    } else {
      for (auto element = buy_map.rbegin(); element != buy_map.rend();
           ++element) {
        if (element->first >= order->price) {
          qty_rem = cross_orders_at_price(order, qty_rem, element->second,
                                          element->first);
        }
      }
      order->quantity = qty_rem;
      if (order->quantity > 0) {
        sell_map[order->price].push_back(order);
      }
    }
  }

public:
  explicit OrderBook(std::vector<Account> &_accounts) : accounts(_accounts) {}

  ~OrderBook() = default;

  double calc_liability(int qty, int price, order_side side) {
    if (side == BUY) {
      return ( // TODO rounding
          ((double)price / (PRICE_MULTIPLIER * 100.0)) *
          (qty / QTY_MULTIPLIER));
    } else {
      return ((1.0 - price / (PRICE_MULTIPLIER * 100.0))) *
             (qty / QTY_MULTIPLIER);
    }
  }

  bool add_order_qty(int account_id, int qty, int price, order_side side) {

    auto liability = calc_liability(qty, price, side);
    auto &account = accounts[account_id];
    if (!account.balance_check(liability)) {
      std::cout << "\nRejecting order as not enough balance!";
      return false;
    }
    account.exposure += liability;
    account.balance -= liability;

    auto order =
        std::make_shared<Order>(account_id, qty, price, side, ++order_seq);
    std::cout << "\n > ORDER: " << *order;
    order_record[order->order_id] = order;
    cross_orders(order);
    return true;
  }

  int quantity_at_price(int price, order_side side) {
    int total_qty = 0;
    if (side == BUY) {
      for (const auto &ord : buy_map[price]) {
        total_qty += ord->quantity;
      }
    } else {
      for (const auto &ord : sell_map[price]) {
        total_qty += ord->quantity;
      }
    }

    return total_qty;
  }

  void formatted_print(int qty, int price) {
    std::cout << double(price) / 100.0 << "%(" << qty << ")\t";
  }

  void pretty_print_book() {
    std::cout << "\n===== ORDER BOOK ===== ";
    std::cout << "\nBUY: ";
    for (auto element = buy_map.rbegin(); element != buy_map.rend();
         ++element) {
      auto buy_qty = quantity_at_price(element->first, BUY);
      if (buy_qty) {
        formatted_print(buy_qty, element->first);
      }
    }

    std::cout << "\nSELL: ";
    for (const auto &element : sell_map) {
      auto sell_qty = quantity_at_price(element.first, SELL);
      if (sell_qty) {
        formatted_print(sell_qty, element.first);
      }
    }
    std::cout << "\n=========================";
  }
};

#endif // ORDERBOOK_ORDER_BOOK_H
