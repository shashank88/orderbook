//
// Created by shashank khare on 17/07/2018.
//

#ifndef ORDERBOOK_ACCOUNT_H
#define ORDERBOOK_ACCOUNT_H

#include <iostream>
#include <string>
class Account {
public: // TODO: add setters and make private
  int id;
  double balance; // TODO: decimal equivalent.
  double exposure;
  double pnl;
  double potential;
  std::string name;

  Account(int id, double balance, std::string name)
      : id(id), balance(balance), exposure(0), pnl(0), potential(0),
        name(name) {}

  bool balance_check(double liability) noexcept { return balance > liability; }

  friend std::ostream &operator<<(std::ostream &os, const Account &acc) {
    os << std::endl
       << acc.name << " => Balance = " << acc.balance
       << ", Exposure = " << acc.exposure
       << ", potential winnings:" << acc.potential;
    return os;
  }
};

#endif // ORDERBOOK_ACCOUNT_H
