cmake_minimum_required(VERSION 3.10)
project(orderbook)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -pedantic -Wextra")


ADD_SUBDIRECTORY(proto)
INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

find_package(Boost 1.67 REQUIRED COMPONENTS system) # For asio
INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})

add_subdirectory(tests)

add_executable(orderbook main.cpp includes/order_book.h includes/Account.h includes/Market.h includes/settlement.h includes/server.h)

TARGET_LINK_LIBRARIES(orderbook proto ${PROTOBUF_LIBRARY} ${Boost_LIBRARIES})